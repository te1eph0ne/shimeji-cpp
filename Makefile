CC=g++
CFLAGS=-Wall `pkg-config --cflags --libs xcb-icccm`

ID=src
OD=bin
OF=shimeji

all: $(ID)/*.cpp $(OD)
	$(CC) $(CFLAGS) $(ID)/*.cpp -o $(OD)/$(OF)
$(OD):
	if ! test -d $(OD) ; then mkdir $(OD) ; fi
clean bin/*:
	rm $(OD)/*
