#ifndef MASCOT_H
#define MASCOT_H

#include <xcb/xcb_icccm.h>

class Mascot {
 public:
  Mascot(xcb_connection_t *connection, 
         const xcb_setup_t *setup);
  ~Mascot();
  xcb_window_t getWindow();
  void meow();
 private:
  xcb_window_t window;
  xcb_screen_iterator_t iterator;
  xcb_screen_t *screen;
  xcb_size_hints_t hints;
};

#endif // MASCOT_H
