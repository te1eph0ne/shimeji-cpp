#include <iostream>
#include "manager.h"
#include "mascot.h"

int main () {
  // Establish a connection to the X client and instantiate Manager
  xcb_connection_t *connection = xcb_connect(NULL, NULL);
  Manager manager(connection);
  // Testing
  manager.addMascot(new Mascot(manager.getConnection(), manager.getSetup()));
  manager.addMascot(new Mascot(manager.getConnection(), manager.getSetup()));
  manager.addMascot(new Mascot(manager.getConnection(), manager.getSetup()));
  manager.addMascot(new Mascot(manager.getConnection(), manager.getSetup()));
  manager.addMascot(new Mascot(manager.getConnection(), manager.getSetup()));
  manager.addMascot(new Mascot(manager.getConnection(), manager.getSetup()));
  manager.addMascot(new Mascot(manager.getConnection(), manager.getSetup()));
  manager.addMascot(new Mascot(manager.getConnection(), manager.getSetup()));
  manager.addMascot(new Mascot(manager.getConnection(), manager.getSetup()));
  manager.addMascot(new Mascot(manager.getConnection(), manager.getSetup()));
  std::cout << "Open windows (ID - index):\n";
  for(unsigned i; i < manager.getNumOfMascots(); ++i) {
    std::cout << manager.getMascot(i)->getWindow() << " - " << i << '\n';
  }
  while(1);
  return 0;
}
