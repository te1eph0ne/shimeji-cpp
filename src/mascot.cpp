#include <iostream>
#include "mascot.h"

#define WID 128
#define HIG 128
#define SCL 1

Mascot::Mascot(xcb_connection_t *connection, const xcb_setup_t *setup) :
window(xcb_generate_id(connection)),
iterator(xcb_setup_roots_iterator(setup)),
screen(iterator.data) {
  xcb_create_window(connection,                    /* Connection          */
                    XCB_COPY_FROM_PARENT,          /* depth               */
                    window,                        /* window Id           */
                    screen->root,                  /* parent window       */
                    0, 0,                          /* x, y                */
                    WID * SCL, HIG * SCL,          /* width, height       */
                    10,                            /* border_width        */
                    XCB_WINDOW_CLASS_INPUT_OUTPUT, /* class               */
                    screen->root_visual,           /* visual              */
                    0, NULL );                     /* masks               */
  xcb_icccm_size_hints_set_base_size(&hints, WID * SCL, HIG * SCL);
  xcb_icccm_size_hints_set_min_size(&hints, WID * SCL, HIG * SCL);
  xcb_icccm_size_hints_set_max_size(&hints, WID * SCL, HIG * SCL);
  xcb_icccm_set_wm_size_hints(connection, 
                              window, 
                              XCB_ATOM_WM_NORMAL_HINTS, 
                              &hints);
  xcb_map_window(connection, window);
}

Mascot::~Mascot() {
  delete this;
}

xcb_window_t Mascot::getWindow() {
  return window;
}
