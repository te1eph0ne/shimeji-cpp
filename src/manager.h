#ifndef MANAGER_H
#define MANAGER_H

#include <xcb/xcb.h>
#include <vector>

class Mascot;

class Manager {
 public:
  Manager(xcb_connection_t *connection);
  ~Manager();
  xcb_connection_t* getConnection();
  const xcb_setup_t* getSetup();
  void addMascot(Mascot *mascot);
  void removeMascot(unsigned long int index);
  void removeMascots(unsigned long int nelems);
  Mascot* getMascot(unsigned long int index);
  std::vector<Mascot*>::size_type getNumOfMascots();
  void update();
 private:
  xcb_connection_t *connection;
  const xcb_setup_t *setup;
  std::vector<Mascot*> mascots;
};

#endif // MANAGER_H
