#include <iostream>
#include <assert.h>
#include "manager.h"
#include "mascot.h"

Manager::Manager(xcb_connection_t *connection) : 
connection(connection), setup(xcb_get_setup(connection)) {}

Manager::~Manager() {
  xcb_disconnect(connection);
}

xcb_connection_t* Manager::getConnection() {
  return connection;
}

const xcb_setup_t* Manager::getSetup() {
  return setup;
}

// Appends a mascot to mascots
void Manager::addMascot(Mascot *mascot) {
  mascots.push_back(mascot);
  update();
}

// Removes mascot at index
void Manager::removeMascot(unsigned long int index) {
  xcb_destroy_window(connection, getMascot(index)->getWindow());
  mascots.erase(mascots.begin() + index);
  update();
}

// Removes last nelems mascots
void Manager::removeMascots(unsigned long int nelems) {
  size_t presize = getNumOfMascots(); // Thanks, User 1263
  for(unsigned long int i = 0; i < nelems; ++i) {
    removeMascot((presize - i) - 1);
  }
}

Mascot* Manager::getMascot(unsigned long int index) {
  return mascots[index];
}

std::vector<Mascot*>::size_type Manager::getNumOfMascots() {
  return mascots.size();
}

void Manager::update() {
  xcb_flush(connection);
}
