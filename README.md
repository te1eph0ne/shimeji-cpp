# shimeji-cpp
A lightweight C++ remix of the shimeji desktop mascot for X11 desktops, utilising the xcb library!
I'm developing this not due to inadequacies in versions already available, but as practice developing efficient, low-level graphical applications for the X window system utilising a barebones toolset (also I'm rather rusty with C++). Additionally, I believe that the support provided by shimeji could be improved by better leveraging modern core system utilities and libraries, making for an all-round cleaner implementation of this classic, lovable desktop mascot.

![](https://camo.githubusercontent.com/e214dda2780c8d9f7e294b6f1f564d46a860a605/687474703a2f2f692e696d6775722e636f6d2f65664879582e706e67)
## Planned features:
- Fully-featured and faithfully accurate to the original program
- Support for both Japanese and English configuration files
- System tray icon/menu with various **fun** features
- The ability to read configurations from a user-dependent directory **(`~/.shimeji`, perhaps?)**
- Support for behaviours unique to each individual style of shimeji
- The ability to manually load any desired shimeji during runtime
___
*If you see somewhere that I made a mistake, or could have done something in a better way, please let me know, I'm only human. <3*